<?php

namespace App\Concerns;

use Illuminate\Http\JsonResponse;

trait BasicResponse
{
    /**
     * Success REST response.
     *
     * @param array  $data
     * @param string $message
     *
     * @return JsonResponse
     */
    public function success($data = [], $message = 'SUCCESS'): JsonResponse
    {
        return response()->json([
            'message' => $message,
            'data' => $data,
        ],200);
    }

    /**
     * Success NO CONTENT response.
     *
     * @return JsonResponse
     */
    public function noContent(): JsonResponse
    {
        return response()->json([
            'message' => 'NO_CONTENT',
            'data' => null,
        ], 204);
    }

    /**
     * Success NO CONTENT response.
     *
     * @param array  $err
     * @param string $message
     * @param boolean $status
     * @return JsonResponse
     */
    public function error($err = [], $message = 'ERROR', $status = 400): JsonResponse
    {
        return response()->json([
            'message' => $message,
            'errors' => $err,
        ], $status);
    }
}
