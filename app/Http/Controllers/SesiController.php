<?php

namespace App\Http\Controllers;

use App\Models\Sesi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SesiController extends Controller
{
    public function index()
    {
        $sesi = Sesi::all();
        return $sesi->toJson();
    }

    public function store()
    {
        $i = DB::table('sesi')->count();
        if ($i == 0) {
            $count = 1;
        } else {
            $sesi = DB::table('sesi')->orderBy('id', 'DESC')->first();
            $count = $sesi->id + 1;
        }

        Sesi::create([
            'name' => 'Sesi' . $count,
            'slug' => 'Sesi' . $count,
            'publish_status' => 0
    ]);

        $msg = [
            'success' => true,
            'massage' => 'Data Sesi Berhasil Dibuat!'
        ];

        return response()->json($msg);
    }

    public function show($id)
    {
        $sesi = Sesi::find($id);

        return $sesi->toJson();
    }

    public function update(Request $request, $id)
    {

        $sesi = Sesi::find($id);
        $sesi->name = $request->name;
        $sesi->slug = $request->name;
        $sesi->publish_status = $request->publish_status;
        $sesi->save();

        $msg = [
            'success' => true,
            'massage' => 'Data Sesi Berhasil Diupdate!'
        ];

        return response()->json($msg);

    }

    public function destroy($id)
    {
        $sesi = Sesi::find($id);
        if(!empty($sesi)){
            $sesi->delete();
            $msg = [
                'success' => true,
                'message' => 'Data Sesi Berhasil Dihapus!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Data Sesi Gagal Dihapus'
            ];
            return response()->json($msg);
        }
    }
}
