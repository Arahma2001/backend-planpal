<?php

namespace App\Http\Controllers;

use App\Models\Dosen;
use Illuminate\Http\Request;

class DosenController extends Controller
{
    public function index()
    {
        $dosen = Dosen::all();
        return $dosen->toJson();
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255|min:3',
            'nip' => 'required|max:11',
            'email' => 'required|max:255|email',
            'bidangKeahlian' => 'required|max:255|min:3'
          ]);

          $dosen = Dosen::create([
            'name' => $validatedData['name'],
            'nip' => $validatedData['nip'],
            'email' => $validatedData['email'],
            'bidangKeahlian' => $validatedData['bidangKeahlian'],
            'slug' => $validatedData['name']
          ]);

        $msg = [
            'success' => true,
            'massage' => 'Data Dosen Berhasil Dibuat!'
        ];

        return response()->json($msg);
    }

    public function show($id)
    {
        $dosen = Dosen::find($id);

        return $dosen->toJson();
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255|min:3',
            'nip' => 'required|max:11',
            'email' => 'required|max:255|email',
            'bidangKeahlian' => 'required|max:255|min:3'
        ]);

        $dosen = Dosen::find($id);
        $dosen->name = $request->name;
        $dosen->nip = $request->nip;
        $dosen->email = $request->email;
        $dosen->bidangKeahlian = $request->bidangKeahlian;
        $dosen->slug = $request->name;
        $dosen->save();

        $msg = [
            'success' => true,
            'massage' => 'Data Dosen Berhasil Diupdate!'
        ];

        return response()->json($msg);

    }

    public function destroy($id)
    {
        $dosen = Dosen::find($id);
        if(!empty($dosen)){
            $dosen->delete();
            $msg = [
                'success' => true,
                'message' => 'Data Dosen Berhasil Dihapus!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Data Dosen Gagal Dihapus'
            ];
            return response()->json($msg);
        }
    }


}
