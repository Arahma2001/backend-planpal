<?php

namespace App\Http\Controllers;

use App\Http\Requests\MahasiswaRequest\StoreMahasiswaRequest;
use App\Http\Requests\MahasiswaRequest\UpdateMahasiswaRequest;
use App\Http\Resources\MahasiswaResource;
use App\Models\Mahasiswa;
use App\Models\Judul;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Str;

class MahasiswaController extends Controller
{
    public function index()
    {
        return request('search')
            ? $this->success($this->search())
            : $this->success($this->indexAllMahasiswa(), 'Successfully index all mahasiswa');
    }

    public function search(){
        $mahasiswa = Mahasiswa::query()->where('name','like','%'.request('search').'%')->get();
        $data = request('sortBy')
            ? (request('desc'))
                ? $mahasiswa->sortByDesc(request('sortBy'))->values()->map(fn($mahasiswa) => new MahasiswaResource($mahasiswa))
                : $mahasiswa->sortBy(request('sortBy'))->values()->map(fn($mahasiswa) => new MahasiswaResource($mahasiswa))
            : $mahasiswa->map(fn($mahasiswa) => new MahasiswaResource($mahasiswa));

        return request('per_page')
            ? $this->paginate($data, request('per_page'))
            : $data;
    }

    public function indexAllMahasiswa(){
        $perPage = is_null(request('per_page')) ? 10 : request('per_page');

        $mahasiswa = request('sortBy')
            ? (request('desc'))
                ? Mahasiswa::all()->sortByDesc(request('sortBy'))->values()->map(fn($mahasiswa) => new MahasiswaResource($mahasiswa))
                : Mahasiswa::all()->sortBy(request('sortBy'))->values()->map(fn($mahasiswa) => new MahasiswaResource($mahasiswa))
            : Mahasiswa::all()->map(fn($mahasiswa) => new MahasiswaResource($mahasiswa));
        return request('per_page')
            ? $this->paginate($mahasiswa, $perPage)
            : $mahasiswa;
        //$mahasiswa = Mahasiswa::all();
        //return $mahasiswa->toJson();
    }

    public function store(StoreMahasiswaRequest $request)
    {
        $validatedData = $request->validated();

        $mahasiswa = Mahasiswa::create([
            'name' => $validatedData['name'],
            'nrp' => $validatedData['nrp'],
            'email' => $validatedData['email'],
            'slug' => Str::slug($validatedData['name'],'-')
        ]);

        return $this->success(new MahasiswaResource($mahasiswa->fresh()));
    }

    public function show($id)
    {
        $mahasiswa = $this->fetchSpesificMahasiswa($id);
        return $this->success(new MahasiswaResource($mahasiswa));
    }

    public function update(UpdateMahasiswaRequest $request, $id)
    {
        $mahasiswa = $this->fetchSpesificMahasiswa($id);
        $mahasiswa->slug = Str::slug($request->name, '-');
        $mahasiswa->update($request->validated());

        return $this->success(new MahasiswaResource($mahasiswa), 'Successfully update mahasiswa !');
    }

    public function destroy(Mahasiswa $mahasiswa)
    {
        return $mahasiswa->delete()
            ? $this->success([], 'Successfully delete mahasiswa !')
            : false;
    }
//Mendapat data Judul dan NRP yang berelasi dengan nama tersebut, hasil : variabel judul dan NRP
    public function findJudul($nama)
    {
        $mahasiswa = Mahasiswa::where('name', 'like', $nama)->get()->first();
        $judul = Judul::where('mahasiswa_id', 'like', $mahasiswa->id)->get()->first();
        $result = [
            'judul' => $judul->name,
            'nrp' => $mahasiswa->nrp,
        ];
        return response()->json($result);
    }

    public function findPembimbing($nrp)
    {
        $mahasiswa = Mahasiswa::where('nrp', 'like', $nrp)->get()->first();
        $judul = Judul::where('mahasiswa_id', 'like', $mahasiswa->id)->get()->first();
        $dosen = $judul->membimbing()->get();

        return $dosen->toJson();
    }

    public function noRoom()
    {
        $mahasiswa = Mahasiswa::whereNull('room_id')->get();

        return $mahasiswa->toJson();
    }

    public function fetchSpesificMahasiswa($id){
        return Mahasiswa::withTrashed()->where('id', $id)->firstOrFail();
    }

}
