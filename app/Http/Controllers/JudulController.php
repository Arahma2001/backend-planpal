<?php

namespace App\Http\Controllers;

use App\Models\Judul;
use Illuminate\Http\Request;

class JudulController extends Controller
{
    public function index()
    {
        $judul = Judul::all();
        return $judul->toJson();
    }

    public function viewDataJudul()
    {
        return view('judul', [
            'judul' => Judul::get(),
        ]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255|min:3'
            ]);

          $judul = Judul::create([
            'name' => $validatedData['name'],
            'slug' => $validatedData['name']
          ]);
          
        $msg = [
            'success' => true,
            'massage' => 'Judul Berhasil Dibuat!'
        ];

        return response()->json($msg);
    }

    public function show($id)
    {
        $judul = Judul::find($id);

        return $judul->toJson();
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255|min:3'
            ]);

        $judul = Judul::find($id);
        $judul->name = $request->name;
        $judul->slug = $request->name;
        $judul->save();

        $msg = [
            'success' => true,
            'massage' => 'Judul Berhasil Diupdate!'
        ];

        return response()->json($msg);

    }

    public function destroy($id)
    {
        $judul = Judul::find($id);
        if(!empty($judul)){
            $judul->delete();
            $msg = [
                'success' => true,
                'message' => 'Judul Berhasil Dihapus!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Judul Gagal Dihapus'
            ];
            return response()->json($msg);
        }
    }
}