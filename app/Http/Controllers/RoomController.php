<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoomController extends Controller
{
    public function index()
    {
        $room = Room::all();
        return $room->toJson();
    }

    public function store()
    {
        $i = DB::table('room')->count();
        if ($i == 0) {
            $count = 1;
        } else {
            $room = DB::table('room')->orderBy('id', 'DESC')->first();
            $count = $room->id + 1;
        }
        Room::create([
            'name' => 'Room' . $count,
            'slug' => 'Room' . $count,
            'publish_status' => 0
    ]);

        $msg = [
            'success' => true,
            'massage' => 'Data Room Berhasil Dibuat!'
        ];

        return response()->json($msg);
    }

    public function show($id)
    {
        $room = Room::find($id);

        return $room->toJson();
    }

    public function update(Request $request, $id)
    {

        $room = Room::find($id);
        $room->name = $request->name;
        $room->slug = $request->name;
        $room->publish_status = $request->publish_status;
        $room->save();

        $msg = [
            'success' => true,
            'massage' => 'Data Room Berhasil Diupdate!'
        ];

        return response()->json($msg);

    }

    public function destroy($id)
    {
        $room = Room::find($id);
        if(!empty($room)){
            $room->delete();
            $msg = [
                'success' => true,
                'message' => 'Data Room Berhasil Dihapus!'
            ];
            return response()->json($msg);
        } else {
            $msg = [
                'success' => false,
                'message' => 'Data Room Gagal Dihapus'
            ];
            return response()->json($msg);
        }
    }

    public function findInfo($id)
    {
        $room = Room::find($id);
        $mahasiswa = $room->mahasiswa()->get()->first();
        $pembimbing = $mahasiswa->pembimbing()->get();
        $penguji = $mahasiswa->penguji()->get();
        $pembimbing = $pembimbing->merge($penguji);

        return $pembimbing->toJson();
    }
}
