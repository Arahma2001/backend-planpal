<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Judul extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'judul';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug', 'mahasiswa_id'
    ];

    public function mahasiswa()
    {
        return $this->belongsTo(Mahasiswa::class);
    }

    public function membimbing()
    {
        return $this->belongsToMany(Dosen::class,'membimbing','judul_id','dosen_id');
    }
}
