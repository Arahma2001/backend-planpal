<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dosen extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'dosen';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'nip',
		'name',
		'isKoor',
        'email',
        'bidangKeahlian',
		'slug',

	];

    public function program()
    {
        return $this->belongsToMany(Program::class,'mengkoordinatori','dosen_id','program_id');
    }

    public function membimbing()
    {
        return $this->belongsToMany(Judul::class,'membimbing','dosen_id','judul_id');
    }

    public function pembimbing()
    {
        return $this->belongsToMany(Mahasiswa::class,'pembimbing','dosen_id','mahasiswa_id');
    }

    public function penguji()
    {
        return $this->belongsToMany(Mahasiswa::class,'penguji','dosen_id','mahasiswa_id');
    }

    public function moderator()
    {
        return $this->belongsToMany(Mahasiswa::class,'moderator','dosen_id','mahasiswa_id');
    }

    /*
    public function keahlian()
    {
        return $this->belongsToMany(BidangKeahlian::class,'keahlian_dosen','bidang_keahlian_id','dosen_id');
    }

    public function program()
    {
        return $this->belongsToMany(Program::class,'mengkoordinatori','program_id','dosen_id');
    }

    public function moderator()
    {
        return $this->hasMany(Mahasiswa::class);
    }

    public function membimbing()
    {
        return $this->belongsToMany(Judul::class,'membimbing','judul_id','dosen_id');
    }

    public function penguji()
    {
        return $this->belongsToMany(Judul::class,'penguji','judul_id','dosen_id');
    }
    */
}
