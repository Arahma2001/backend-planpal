<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mahasiswa extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'mahasiswa';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'nrp', 'slug','email','room_id', 'sesi_id'
    ];

    public function judul()
    {
        return $this->hasOne(Judul::class);
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function sesi()
    {
        return $this->belongsTo(Sesi::class);
    }

    public function pembimbing()
    {
        return $this->belongsToMany(Dosen::class,'pembimbing','mahasiswa_id','dosen_id');
    }

    public function penguji()
    {
        return $this->belongsToMany(Dosen::class,'penguji','mahasiswa_id','dosen_id');
    }

    public function moderator()
    {
        return $this->belongsToMany(Dosen::class,'moderator','mahasiswa_id','dosen_id');
    }
}
