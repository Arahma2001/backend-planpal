<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Tightenco\Ziggy\Ziggy;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//ZIGGY
Route::get('api-route-list', fn () => response()->json(new Ziggy));

//Route CRUD Dosen
Route::get('/dosen', 'App\Http\Controllers\DosenController@index')->name('dosen.index');
Route::post('/dosen/store', 'App\Http\Controllers\DosenController@store')->name('dosen.store');
Route::get('/dosen/show/{dosen}', 'App\Http\Controllers\DosenController@show')->name('dosen.show');
Route::put('/dosen/update/{dosen}', 'App\Http\Controllers\DosenController@update')->name('dosen.update');
Route::delete('/dosen/delete/{dosen}', 'App\Http\Controllers\DosenController@destroy')->name('dosen.destroy');

//Route CRUD Mahasiswa
Route::get('/mahasiswa', 'App\Http\Controllers\MahasiswaController@index')->name('mahasiswa.index');
Route::post('/mahasiswa/store', 'App\Http\Controllers\MahasiswaController@store')->name('mahasiswa.store');
Route::get('/mahasiswa/show/{mahasiswa}', 'App\Http\Controllers\MahasiswaController@show')->name('mahasiswa.show');
Route::put('/mahasiswa/update/{mahasiswa}', 'App\Http\Controllers\MahasiswaController@update')->name('mahasiswa.update');
Route::delete('/mahasiswa/delete/{mahasiswa}', 'App\Http\Controllers\MahasiswaController@destroy')->name('mahasiswa.destroy');

//Route CRUD Room
Route::get('/room', 'App\Http\Controllers\RoomController@index')->name('room.index');
Route::post('/room/store', 'App\Http\Controllers\RoomController@store')->name('room.store');
Route::get('/room/show/{room}', 'App\Http\Controllers\RoomController@show')->name('room.show');
Route::put('/room/update/{room}', 'App\Http\Controllers\RoomController@update')->name('room.update');
Route::delete('/room/delete/{room}', 'App\Http\Controllers\RoomController@destroy')->name('room.destroy');

//Route CRUD Sesi
Route::get('/sesi', 'App\Http\Controllers\SesiController@index')->name('sesi.index');
Route::post('/sesi/store', 'App\Http\Controllers\SesiController@store')->name('sesi.store');
Route::get('/sesi/show/{sesi}', 'App\Http\Controllers\SesiController@show')->name('sesi.show');
Route::put('/sesi/update/{sesi}', 'App\Http\Controllers\SesiController@update')->name('sesi.update');
Route::delete('/sesi/delete/{sesi}', 'App\Http\Controllers\SesiController@destroy')->name('sesi.destroy');
//Route CRUD Judul
Route::get('/judul', 'App\Http\Controllers\JudulController@index')->name('judul.index');
Route::post('/judul/store', 'App\Http\Controllers\JudulController@store')->name('judul.store');
Route::get('/judul/show/{judul}', 'App\Http\Controllers\JudulController@show')->name('judul.show');
Route::put('/judul/update/{judul}', 'App\Http\Controllers\JudulController@update')->name('judul.update');
Route::delete('/judul/delete/{judul}', 'App\Http\Controllers\JudulController@destroy')->name('judul.destroy');

//Costum Route CRUD
Route::get('/mahasiswa/find/judul/{nama}', 'App\Http\Controllers\MahasiswaController@findJudul')->name('mahasiswa.judul.find');
Route::get('/mahasiswa/find/dosen/{nrp}', 'App\Http\Controllers\MahasiswaController@findPembimbing')->name('mahasiswa.pembimbing.find');
Route::get('/mahasiswa/noroom', 'App\Http\Controllers\MahasiswaController@noRoom')->name('mahasiswa.noroom');
Route::get('/room/find/info/{room}', 'App\Http\Controllers\RoomController@findInfo')->name('room.indo.find');
