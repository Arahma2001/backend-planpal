<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use App\Models\Dosen;

class DosenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        $bidahli = ['Jaringan', 'Web', 'Mobile'];
        for($i=0;$i<15;$i++){
            $nama = $faker->name;
            $randahli = Arr::random($bidahli);
            Dosen::create([
                'name' => $nama,
                'nip' => 537243736 + (100 * $i) + $i,
                'email' => Str::slug($nama, '.') . '@gmail.com',
                'bidangKeahlian' => $randahli,
                'slug' => Str::slug($nama, '-'),
        ]);

        }

    }
}
