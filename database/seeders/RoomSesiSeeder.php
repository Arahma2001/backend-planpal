<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use App\Models\Room;
use App\Models\Sesi;

class RoomSesiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<6;$i++){
            $count = DB::table('room')->count();
            Room::create([
                'name' => 'Room' . ($count + 1),
                'slug' => 'Room' . ($count + 1),
                'publish_status' => 0
        ]);

            Sesi::create([
                'name' => 'Sesi' . ($count + 1),
                'slug' => 'Sesi' . ($count + 1),
                'publish_status' => 0
    ]);
        }

    }
}
