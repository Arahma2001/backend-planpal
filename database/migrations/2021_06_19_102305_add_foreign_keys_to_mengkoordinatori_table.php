<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToMengkoordinatoriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mengkoordinatori', function (Blueprint $table) {
            $table->foreign('dosen_id')->references('id')->on('dosen')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('program_id')->references('id')->on('program')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mengkoordinatori', function (Blueprint $table) {
            $table->dropForeign('mengkoordinatori_dosen_id_foreign');
            $table->dropForeign('mengkoordinatori_program_id_foreign');
        });
    }
}
