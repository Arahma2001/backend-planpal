<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToMahasiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mahasiswa', function (Blueprint $table) {
            $table->foreign('room_id', 'mahasiswa_ibfk_1')->references('id')->on('room')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('sesi_id', 'mahasiswa_ibfk_2')->references('id')->on('sesi')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mahasiswa', function (Blueprint $table) {
            $table->dropForeign('mahasiswa_ibfk_1');
            $table->dropForeign('mahasiswa_ibfk_2');
        });
    }
}
