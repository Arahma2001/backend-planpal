<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMahasiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahasiswa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('room_id')->nullable();
            $table->unsignedBigInteger('sesi_id')->nullable();
            $table->integer('nrp')->unique();
            $table->string('name');
            $table->string('slug');
            $table->string('email')->unique();
            $table->softDeletes();
            $table->timestamps();
            $table->unique(['room_id', 'sesi_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahasiswa');
    }
}
