<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToPembimbingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pembimbing', function (Blueprint $table) {
            $table->foreign('dosen_id', 'pembimbing_ibfk_1')->references('id')->on('dosen')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('mahasiswa_id', 'pembimbing_ibfk_2')->references('id')->on('mahasiswa')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pembimbing', function (Blueprint $table) {
            $table->dropForeign('pembimbing_ibfk_1');
            $table->dropForeign('pembimbing_ibfk_2');
        });
    }
}
