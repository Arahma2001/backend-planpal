<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembimbingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membimbing', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('judul_id');
            $table->unsignedBigInteger('dosen_id')->index('membimbing_dosen_id_foreign');
            $table->timestamps();
            $table->unique(['judul_id', 'dosen_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membimbing');
    }
}
