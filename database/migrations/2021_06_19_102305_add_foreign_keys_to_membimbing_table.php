<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToMembimbingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('membimbing', function (Blueprint $table) {
            $table->foreign('dosen_id', 'membimbing_ibfk_1')->references('id')->on('dosen')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('judul_id')->references('id')->on('judul')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('membimbing', function (Blueprint $table) {
            $table->dropForeign('membimbing_ibfk_1');
            $table->dropForeign('membimbing_judul_id_foreign');
        });
    }
}
